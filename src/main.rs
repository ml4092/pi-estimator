// Estimates pi using the Monte Carlo Method
// https://www.101computing.net/estimating-pi-using-the-monte-carlo-method/

extern crate sdl2;

use rand::distributions::Uniform;
use rand::prelude::{ThreadRng, Distribution};
use rand::Rng;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::ttf::Font;
use sdl2::video::Window;

// TODO: Plot Results

const COEFFICIENT: f32 = 4.0;

// Graphics constants
const BACKGROUND: Color = Color::GRAY;
const CANVAS_SIZE: u32 = 800;
const POINT_SIZE: u32 = 5;

const UPPER_BOUND: i32 = (CANVAS_SIZE / 2) as i32 / POINT_SIZE as i32;

struct NumberGenerator {
    uniform: Option<Uniform<i32>>,
    rng: ThreadRng
}

impl NumberGenerator {
    fn from(args: &Vec<String>) -> Self {
        Self {
            rng: rand::thread_rng(),
            uniform: if args.contains(&"-u".to_string())
                || args.contains(&"--uniform".to_string())
            {
                Some(Uniform::from(-UPPER_BOUND..UPPER_BOUND))
            } else {
                None
            }
        }
    }

    fn get(&mut self) -> i32 {
        match self.uniform {
            Some(u) => u.sample(&mut self.rng),
            None => self.rng.gen_range(-UPPER_BOUND..UPPER_BOUND)
        }
    }
}

struct CirclePoint {
    point: Rect,
    in_circle: bool
}

impl CirclePoint {
    fn new(x: i32, y: i32, z: i32) -> Self {
        CirclePoint {
           point: Rect::new(x, y, POINT_SIZE, POINT_SIZE),
           in_circle: x * x + y * y <= z * z
        }
    }
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let mut gen = NumberGenerator::from(&args);

    let context = sdl2::init().unwrap();
    let video = context
        .video()
        .unwrap();
    let window = video
        .window("Pi Visualizer", CANVAS_SIZE, CANVAS_SIZE)
        .position_centered()
        .build()
        .unwrap();
    let mut canvas = window
        .into_canvas()
        .present_vsync()
        .build()
        .unwrap();
    let mut event_pump = context
        .event_pump()
        .unwrap();

    let mut points: Vec<CirclePoint> = Vec::new();

    canvas.set_draw_color(BACKGROUND);
    canvas.clear();

    let ttf = sdl2::ttf::init().unwrap();
    let font = ttf
       .load_font("/Library/Fonts/Arial Unicode.ttf", 20)
       .unwrap();

    canvas.present();

    let mut running = true;
    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } |
                Event::KeyDown { keycode: Some(Keycode::Q), .. } => {
                    running = false;
                },
                Event::KeyDown { keycode: Some(Keycode::Space), .. } => {
                    canvas.set_draw_color(BACKGROUND);
                    canvas.clear();

                    let inner = draw_points(
                        &mut canvas,
                        &mut points,
                        &mut gen
                    ) as f32;
                    let total = points.len() as f32;
                    let pi = COEFFICIENT * inner / total;

                    let height = font.height();
                    let text = format!("n = {}", total);
                    blit_text(
                        &mut canvas,
                        &text,
                        &font,
                        0,
                        CANVAS_SIZE as i32 - 2 * height
                    );

                    let text = format!("pi = {}", pi);
                    blit_text(
                        &mut canvas,
                        &text,
                        &font,
                        0,
                       CANVAS_SIZE as i32 - height
                    );

                    canvas.present();
                },
                _ => {}
            }
        }
    }
}

fn blit_text(
    canvas: &mut Canvas<Window>,
    text: &String,
    font: &Font,
    x: i32,
    y: i32
)
{
    let texture_creator = canvas.texture_creator();

    let surface = font
        .render(text)
        .blended(Color::YELLOW)
        .unwrap();
    let texture = texture_creator
        .create_texture_from_surface(&surface)
        .unwrap();

    let text_height = surface.height();
    let target = Rect::new(
        x,
        y,
        texture.query().width,
        text_height
    );
    canvas.copy(&texture, None, Some(target)).unwrap();
}

fn draw_points(
    canvas: &mut Canvas<Window>,
    points: &mut Vec<CirclePoint>,
    gen: &mut NumberGenerator
) -> u32
{
    let x: i32 = gen.get() * POINT_SIZE as i32;
    let y: i32 = gen.get() * POINT_SIZE as i32;

    points.push(CirclePoint::new(x, y, UPPER_BOUND));

    let mut points_in_circle = 0;

    for p in points {
        let x = p.point.x + UPPER_BOUND;
        let y = p.point.y + UPPER_BOUND;

        if p.in_circle {
            canvas.set_draw_color(Color::WHITE);
            points_in_circle += 1;
        }
        else {
            canvas.set_draw_color(Color::BLACK);
        }

        canvas.fill_rect(Rect::new(x, y, POINT_SIZE, POINT_SIZE)).unwrap();
    }

    points_in_circle
}

