# Pi Estimator

Estimates the value of pi using the [Monte Carlo Method](https://www.101computing.net/estimating-pi-using-the-monte-carlo-method/).

